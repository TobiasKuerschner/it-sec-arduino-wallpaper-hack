#include <DigiKeyboardDe.h>

void setup() {
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
}

// method to shorten delay
void w(int _d) {
  DigiKeyboardDe.delay(_d*1000);
}

// method to shorten print method and wait 1 sec
void p(fstr_t* _t) {
  DigiKeyboardDe.println(_t);
  w(1);
}

void loop() {
  // initialize keyboard, when keyboard ready both led's are on
  DigiKeyboardDe.sendKeyStroke(0);
  w(1);
  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);
  // minimalize open windows
  DigiKeyboardDe.sendKeyStroke(KEY_M, MOD_GUI_LEFT);
  w(1);
  // open run prompt
  DigiKeyboardDe.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  w(1);
  // open powershell window with admin proviliges
  p(F("powershell Start-Process powershell -Verb runAs"));
  w(1);
  // accept admin prompt
  DigiKeyboardDe.sendKeyStroke(KEY_J, MOD_ALT_LEFT);
  w(1);
  // minimalize powershell window
  p(F("mode con:cols=18 lines=1"));
  // set variables
  p(F("$url = ‘https://i.imgflip.com/1dv8ac.jpg’;"));
  p(F("$path = ‘C:\\test.jpg’;"));
  // download image
  p(F("Invoke-WebRequest $url -OutFile $path"));
  // set wallpaper
  p(F("Function Set-WallPaper($value) { Set-ItemProperty -path 'HKCU:\\Control Panel\\Desktop\\' -name wallpaper -value $value; rundll32.exe user32.dll, UpdatePerUserSystemParameters; rundll32.exe user32.dll, UpdatePerUserSystemParameters; }"));
  p(F("Set-WallPaper -value $path"));
  // close window
  p(F("exit"));
  // when script is finished both led's are off
  digitalWrite(0, LOW);
  digitalWrite(1, LOW);
  w(2);
}
